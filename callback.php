<?php
	session_start();

	if (isset($_GET['error'])) {
		header('Location: login.php');
		exit();
	}

	require "InstagramAPI.php";
		include "database.php";
	$data = $Instagram->getAccessTokenAndUserDetails($_GET['code']);

	$_SESSION['loggedIn'] = 1;
	$_SESSION['accessToken'] = $data['access_token'];
	$_SESSION['id'] = $data['user']['id'];
	$_SESSION['username'] = $data['user']['username'];
	$_SESSION['bio'] = $data['user']['bio'];
	$_SESSION['website'] = $data['user']['website'];
	$_SESSION['fullName'] = $data['user']['full_name'];
	$_SESSION['profilePicture'] = $data['user']['profile_picture'];

	if($db->RegistertInstagramUser($_SESSION['username'],$_SESSION['id'],$_SESSION['fullName'],$_SESSION['accessToken']))
	{
		echo '<script type="text/javascript"> window.location.replace("dashboard.php") </script>';
		exit();
	}else{

	}
?>
