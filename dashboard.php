<?php include('header.php');?>
<?php include "database.php";?>
<?php

  //$IGusername= "fedepasi94";
  //$IgId = 241067845;
  if(!isset($_COOKIE["login"]))
  {
    echo '<script type="text/javascript">window.location.replace("login.php") </script>';
  }

  if(isset($_POST["username"]))
  {
  $IGusername = $_POST["username"];
  // Create connection
  $user = $db->insertUser($IGusername);
  $IgId = getId($user);
	$engagement_rate = calculate_engagement_rate($user);
  $follower = getFollower($user);
  $engagement_positivo = calculate_vote_engagement_rate($follower,$engagement_rate);
?>
  <!-- Page Content -->
  <!-- ============================================================== -->
  <div id="page-wrapper" style="background-image:url(<?php echo get_first_media($user)?>); background-repeat: no-repeat;background-size: cover; background-attachment: fixed; padding-bottom:0px;">
      <div class="container-fluid" style="background-color: rgba(0, 0, 0, 0.6);padding-bottom: 40px;">
          <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">Dashboard</h4>
              </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active">Instalytics</li>
                </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>
        <!-- ============================================================== -->
        <!-- Profile, & inbox widgets -->
        <!-- ============================================================== -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-6 col-sm-12 col-lg-offset-1 col-lg-5" data-aos="fade-up">
                <div class="panel ig-profile">
                    <div class="p-30">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4"><img src="<?php echo getProfilePic($user)?>" alt="<?php echo getFullName($user)?>" class="img-circle img-responsive center-mobile"></div>
                            <div class="col-xs-12 col-sm-8">
                                <h2 class="m-b-0 center-mobile"><?php echo getFullName($user)?></h2>
                                <h4 class="center-mobile"><?php echo getBusinessCategory($user)?></h4>
                            </div>
                        </div>
                        <div class="row text-center m-t-30 stats">
                            <div class="col-xs-4 b-r"style="padding: 0px 5px;">
                                <h2><?php echo getNumMedia($user)?></h2>
                                <h4>MEDIA</h4>
                            </div>
                            <div class="col-xs-4 b-r" style="padding: 0px 5px;">
                                <h2><?php echo $follower?></h2>
                                <h4>FOLLOWER</h4>
                            </div>
                            <div class="col-xs-4" style="padding: 0px 5px;">
                                <h2><?php echo getFollowing($user)?></h2>
                                <h4>FOLLOWING</h4>
                            </div>
                        </div>
                    </div>
                    <hr class="m-t-10" />
                    <div class="p-20 text-center">
                        <p class=""><?php echo getBiography($user)?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-lg-5" data-aos="fade-up">
                <div class="panel">
                    <div class="p-30">
                      <div class="row text-center m-t-30">
                          <div class="col-xs-6 b-r">
                            <h2><?php echo  round(calculate_media_like($user));?></h2>
                            <h4>MEDIA LIKE</h4>
                          </div>
                          <div class="col-xs-6">
                            <h2><?php echo  round(calculate_media_commenti($user));?></h2>
                            <h4>MEDIA COMMENTI</h4>
                          </div>
                      </div>
                      <hr class="m-t-10" />
                      <div class="row text-center m-t-30">
                          <div class="col-xs-offset-3 col-xs-6">
                              <h2><?php echo round($engagement_rate,2);?>%</h2>
                              <h4>ENGAGEMENT RATE</h4>
                          </div>
                      </div>
                      <hr class="m-t-10" />
                      <div class="row text-center m-t-20">
                          <div class="text-center">
                            <p class="">
                              <?php if($engagement_positivo)
                              {
                                echo '<div class="green p-20 " style="border: solid 4px #77dd77;border-radius: 10px;">
                                ENGAGEMENT RATE più alto rispetto alla media
                                </div>';
                              }else{
                                echo '<div class="red p-20 " style="border: solid 4px #dd7777;border-radius: 10px;">
                                ENGAGEMENT RATE più basso rispetto alla media
                                </div>';
                              }
                              ?>
                            </p>
                          </div>
                        </div>
                    </div>
                    <!-- <div class="p-20 text-center">
                        <hr>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- row -->
        <div class="row">
            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12"data-aos="fade-up">
                <div class="white-box">
                    <h3 class="box-title">Follower</h3>
                    <div class="chart-container">
                        <canvas id="FollowerChart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12" data-aos="fade-up">
                <div class="white-box">
                    <h3 class="box-title">Following</h3>
                    <div class="chart-container">
                        <canvas id="FollowingChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12"data-aos="fade-up">
                <div class="white-box">
                    <h3 class="box-title">Like</h3>
                    <div class="chart-container">
                        <canvas id="PostLikeChart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12" data-aos="fade-up">
                <div class="white-box">
                    <h3 class="box-title">Commenti</h3>
                    <div class="chart-container">
                        <canvas id="PostCommentChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-sm-12" data-aos="fade-up">
              <div class="white-box">
                  <h3 class="box-title m-b-0 text-center">Analisi Dettagliata</h3>
                  <p class="text-muted m-b-30 text-center">Una semplice tabella che può però rivelarti molto della strategia di un profilo! <br />
                    SUGGERIMENTO: Controlla se la crescita corrisponde alla pubblicazione di un post. Se si, vai a vedere cosa c'è in quel post!</p>
                  <div class="table-responsive">
                    <?php
                      $userLog = $db->GetUserLog($IgId);
                      if (mysqli_num_rows($userLog) > 0) {
                    ?>
                      <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th style="width: 100px;">Data</th>
                                  <th>Follower</th>
                                  <th>New Follower</th>
                                  <th>Following</th>
                                  <th>New Following</th>
                                  <th>Media</th>
                                  <th>New Media</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                      					// output data of each row
                      					$trovato = false;
                                $row = null;
                                $rowBefore = null;
                                $newFollower = 0;
                                $newFollowing = 0;
                                $newMedia = 0;
                      					while($row = mysqli_fetch_assoc($userLog)) {
                                  if($rowBefore == null)
                                  {
                                    $rowBefore = $row;
                                  }else{
                                    $newFollower = ($row["follower"] - $rowBefore["follower"]);
                                    $newFollowing = ($row["following"] - $rowBefore["following"]);
                                    $newMedia = ($row["num_media"] - $rowBefore["num_media"]);
                                    $rowBefore = $row;
                                  }
                                  echo "<tr>";
                                  echo "<td>".$row["log_date"]."</td>";
                                  echo "<td>".$row["follower"]."</td>";
                                  echo "<td class=".($newFollower > 0 ? 'green' : 'red').">".($newFollower > 0 ? '+' : '').$newFollower."</td>";
                                  echo "<td>".$row["following"]."</td>";
                                  echo "<td class=".($newFollowing > 0 ? 'green' : 'red').">".($newFollowing > 0 ? '+' : '').$newFollowing."</td>";
                                  echo "<td>".$row["num_media"]."</td>";
                                  echo "<td class=".($newMedia > 0 ? 'green' : 'red').">".($newMedia > 0 ? '+' : '').$newMedia."</td>";
                                  echo "</tr>";
                      					}
                                ?>
                          </tbody>
                      </table>
                    <?php
                  }else{

                  }
                    ?>
                  </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- row -->
        <div class="row el-element-overlay">
          <?php
            $c = 0;
            $size = count($user->edge_owner_to_timeline_media->edges);
            while ($c<$size)
            {
              $url = $user->edge_owner_to_timeline_media->edges[$c]->node->thumbnail_src;
              $url_big = $user->edge_owner_to_timeline_media->edges[$c]->node->display_url;
              $comment = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_media_to_comment->count;
              $like = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_liked_by->count;
            ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" data-aos="zoom-in-up">
                <div class="white-box">
                    <div class="el-card-item">
                        <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $url?>" class="img-responsive" />
                            <div class="el-overlay">
                                <ul class="el-info">
                                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $url_big ?>"><i class="icon-magnifier"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="el-card-content">
                            <h3 class="box-title"><i class="fas fa-heart"></i> <?php echo $like?></h3> <small><i class="far fa-comment"></i> <?php echo $comment ?></small>
                            <br/> </div>
                    </div>
                </div>
            </div>
            <?php
              $c=$c+1;
            }
          ?>
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- Blog-component -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xs-12 col-sm-6"> <img class="img-responsive" alt="user" src="plugins/images/big/img1.jpg">
                <div class="white-box">
                    <div class="text-muted"><span class="m-r-10"><i class="icon-calender"></i> May 16</span> <a class="text-muted m-l-10" href="#"><i class="fa fa-heart-o"></i> 38</a></div>
                    <h3 class="m-t-20 m-b-20">Top 20 Models are on the ramp</h3>
                    <p>Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Read more</button>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xs-12 col-sm-6"> <img class="img-responsive" alt="user" src="plugins/images/big/img2.jpg">
                <div class="white-box">
                    <div class="text-muted"><span class="m-r-10"><i class="icon-calender"></i> May 16</span> <a class="text-muted m-l-10" href="#"><i class="fa fa-heart-o"></i> 38</a></div>
                    <h3 class="m-t-20 m-b-20">Top 20 Models are on the ramp</h3>
                    <p>Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Read more</button>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xs-12 col-sm-6"> <img class="img-responsive" alt="user" src="plugins/images/big/img3.jpg">
                <div class="white-box">
                    <div class="text-muted"><span class="m-r-10"><i class="icon-calender"></i> May 16</span> <a class="text-muted m-l-10" href="#"><i class="fa fa-heart-o"></i> 38</a></div>
                    <h3 class="m-t-20 m-b-20">Top 20 Models are on the ramp</h3>
                    <p>Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Read more</button>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xs-12 col-sm-6"> <img class="img-responsive" alt="user" src="plugins/images/big/img4.jpg">
                <div class="white-box">
                    <div class="text-muted"><span class="m-r-10"><i class="icon-calender"></i> May 16</span> <a class="text-muted m-l-10" href="#"><i class="fa fa-heart-o"></i> 38</a></div>
                    <h3 class="m-t-20 m-b-20">Top 20 Models are on the ramp</h3>
                    <p>Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Read more</button>
                </div>
            </div>
        </div>
        <?php include('right-sidebar.php');?>
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Ample Admin brought to you by themedesigner.in </footer>
</div>
<?php }else{
  echo('<div id="page-wrapper"><h1 style="margin:120px;">Cerca un utente</h1></div>');
} //end if(isset($IGusername = $_POST["username"]))?>
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<?php include('footer.php');?>
<!-- <script>
window.addEventListener("beforeprint", function(event) { ... });
window.onbeforeprint = function beforePrintHandler () {
  for (var id in Chart.instances) {
    Chart.instances[id].resize()
  }
}
</script> -->
<?php
  $result = $db->GetLast30Days($IgId);
  $follower = array();
  $following = array();
  $max_follower = 0;
  $max_following = 0;
  $min_follower = 0;
  $min_following = 0;
  $num_post = 0;
  $log_date = array();
  $c = 0;
  if (mysqli_num_rows($result) > 0) {
    // output data of each row
    echo($c);
    while($row = mysqli_fetch_assoc($result)) {
      array_push($follower,$row["follower"]);
      array_push($following,$row["following"]);
      array_push($log_date,$row["log_date"]);
      $c = $c + 1;
    }
  }
  $c = 0;
  $size = count($user->edge_owner_to_timeline_media->edges);
  $likes = array();
  $media = array();
  $comments = array();
  while ($c<$size)
  {
    $comment = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_media_to_comment->count;
    $like = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_liked_by->count;
    array_push($likes,$like);
    array_push($comments,$comment);
    array_push($media,$c);
    $c=$c+1;
  }
?>
<script>
  <?php echo 'var follower = ' . json_encode($follower) . ';';?>
  <?php echo 'var following = ' .json_encode($following). ';';?>
  <?php echo 'var likes = ' . json_encode($likes) . ';';?>
  <?php echo 'var comments = ' . json_encode($comments) . ';';?>
  <?php echo 'var num_media = ' .json_encode($media). ';';?>
  <?php echo 'var date = ' . json_encode($log_date) . ';';?>
		var configFollower = {
			type: 'line',
			data: {
				labels: date,
				datasets: [{
					label: 'Follower',
					backgroundColor: '#a4d1e4',
					borderColor: '#2cabe3',
					data: follower,
					fill: false,
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: ''
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Day'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Follower'
						}
            // ticks: {
            //   max: max_follower
            // }
					}]
				}
			}
		};
    var configFollowing = {
      type: 'line',
      data: {
        labels: date,
        datasets: [{
          label: 'Following',
          backgroundColor: '#ffd7d7',
          borderColor: '#ff7676',
          data: following,
          fill: false,
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: ''
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Day'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Following'
            }
            // ticks: {
            //   max: max_follower
            // }
          }]
        }
      }
    };
    var configPostComment = {
      type: 'line',
      data: {
        labels: num_media,
        datasets: [{
          label: 'Commenti',
          backgroundColor: '#ffd7d7',
          borderColor: '#ff7676',
          data: comments,
          fill: false,
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: ''
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Ultimi Post'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Commenti'
            }
          }]
        }
      }
    };
    var configPostLike = {
			type: 'line',
			data: {
				labels: num_media,
				datasets: [{
					label: 'Like',
					backgroundColor: '#a4d1e4',
					borderColor: '#2cabe3',
					data: likes,
					fill: false,
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: ''
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Ultimi Post'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Like'
						}
					}]
				}
			}
		};
		window.onload = function() {
			var ctx = document.getElementById('FollowerChart').getContext('2d');
			window.myLine = new Chart(ctx, configFollower);
      var ctxFoll = document.getElementById('FollowingChart').getContext('2d');
      window.myLine = new Chart(ctxFoll, configFollowing);
			var ctxLike = document.getElementById('PostLikeChart').getContext('2d');
			window.myLine = new Chart(ctxLike, configPostLike);
      var ctxComm = document.getElementById('PostCommentChart').getContext('2d');
      window.myLine = new Chart(ctxComm, configPostComment);
		};
	</script>
  <script src="plugins/bower_components/datatables/datatables.min.js"></script>
  <!-- start - This is for export functionality only -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  <!-- end - This is for export functionality only -->
  <script>
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "displayLength": 25,
        "order": [[ 1, 'desc' ]],
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary m-r-10');
  </script>
<?php include('end.php');?>
