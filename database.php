<?php
	include "functions.php";
	include "settings.php";
	$db = new Database_functions($sett_nomeuser,$sett_password);
  class Database_functions
  {
    // parametri per la connessione al database
    public $nomehost = "localhost";
    // public $nomeuser = "usr_fp_test";
    // public $password = "dy@3A63q";
    public $nomeuser = "";
    public $password = "";
    // nome del database da selezionare
    public $nomedb = "fp_test";
    // controllo sulle connessioni attive
    private $attiva = false;
    // funzione per la connessione a MySQL
    public function __construct($_nomeuser,$_password) {
      //
	    $this->nomeuser = $_nomeuser;
	    $this->password = $_password;
    }
    // funzione per la connessione a MySQL
    public function connetti()
    {
			// Create connection
			$conn = new mysqli($this->nomehost, $this->nomeuser, $this->password);
			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			}
			return $conn;
    }
    //funzione per l'esecuzione delle query
    public function query($sql)
    {
			$conn = $this->connetti();
		    //$result = ->query($sql);
        echo mysqli_query($conn, $sql);
				$this->disconnetti($conn);
    }
      // funzione per la chiusura della connessione
    public function disconnetti($conn)
    {
			mysqli_close($conn);
    }

    //funzione per l'inserimento dei dati dell'utente, passo direttamente il JSON
    public function insertUser($IGusername) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$user = getIgProfile($IGusername);
			$ig_id = getId($user);
			$usernm = getUsername($user);
			$full_name = getFullName($user);
			$full_name = str_replace("'"," ",$full_name);
			$sql = "SELECT id,full_name,ig_id,username,last_update FROM user WHERE ig_id = $ig_id";
			$result = mysqli_query($conn, $sql);
			if (mysqli_num_rows($result) <= 0) {
					// output data of each row
					$trovato = false;
							$date = date("Y-m-d");
							$sql = "INSERT INTO user (id, username, ig_id, full_name, register_date,last_JSON,last_update)  VALUES (NULL,'$usernm','$ig_id','$full_name',CAST('". $date ."' AS DATE),null,CAST('". $date ."' AS DATE))";

							if ($conn->query($sql) === TRUE) {
								 echo "New record created successfully";
							} else {
								 echo "Error: " . $sql . "<br>" . $conn->error;
							}
			} else {
					echo "User Found";
			}
			return $user;
			mysqli_close($conn);
		}
		public function GetListUser() {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$sql = "SELECT id,full_name,ig_id,username FROM user";
			$result = mysqli_query($conn, $sql);
			return $result;
			mysqli_close($conn);
		}
		public function GetUserByIgId($IgId) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$sql = "SELECT id,full_name,ig_id,username FROM user WHERE ig_id = $IgId";
			$result = mysqli_query($conn, $sql);
			return $result;
		}
		public function GetLast30Days($IgId) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$sql = "SELECT follower,following,log_date FROM userlog WHERE ig_id = $IgId";
			$result = mysqli_query($conn, $sql);
			return $result;
		}
		public function GetUserLog($IgId) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$sql = "SELECT id,follower,following,num_media,log_date FROM userlog WHERE ig_id = $IgId";
			$result = mysqli_query($conn, $sql);
			return $result;
		}
		//funzione per l'inserimento dei dati dell'utente, passo direttamente il JSON
		public function RegistertUser($email,$password,$cpassword) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			if($password==$cpassword)
			{
				$date = date("Y-m-d");
				$encrypted_password = md5($password);

				$query= "select * from user WHERE email='$email'";
				$query_run = mysqli_query($conn,$query);

				if(mysqli_num_rows($query_run)>0)
				{
					// there is already a user with the same username
					echo '<script type="text/javascript"> alert("User already exists.. try another mail") </script>';
				}
				else
				{
					$sql = "INSERT INTO user (id, username, ig_id, full_name, register_date,last_JSON,last_update,password,email)  VALUES (NULL,'','','',CAST('". $date ."' AS DATE),' ',CAST('". $date ."' AS DATE),'$encrypted_password','$email')";

					if ($conn->query($sql) === TRUE) {
		 				return true;
					} else {
						return false;
					}
				}
			}
			else{
				echo '<script type="text/javascript"> console.log("Password and confirm password does not match!") </script>';
			}
			mysqli_close($conn);
		}

		//funzione per l'inserimento dei dati dell'utente, passo direttamente il JSON
		public function RegistertInstagramUser($username,$IgId,$fullname,$igToken) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
				$date = date("Y-m-d");
				$query= "select username from user WHERE username='$username'";
				$query_run = mysqli_query($conn,$query);

				if(mysqli_num_rows($query_run)>0)
				{
					// there is already a user with the same username
					echo 'trovato utente';
						session_start();
						$_SESSION['username'] = $row["username"];
				}
				else
				{
				echo 'nessun utente trovato';
					$sql = "INSERT INTO user (id, username, ig_id, full_name, register_date,last_JSON,last_update,password,email,ig_token)  VALUES (NULL,'$username','$IgId','$fullname',CAST('". $date ."' AS DATE),' ',CAST('". $date ."' AS DATE),'','','$igToken')";
					if ($conn->query($sql) === TRUE) {
		 				return true;
					} else {
						echo($conn->error);
						return false;
					}
				}
			mysqli_close($conn);
		}
		//funzione per l'inserimento dei dati dell'utente, passo direttamente il JSON
		public function Loginuser($email,$password) {
			$conn = mysqli_connect($this->nomehost, $this->nomeuser, $this->password, $this->nomedb);
			// Check connection
			if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
			}
			$encrypted_password = md5($password);

			$query= "select email from user WHERE email='$email' && password='$encrypted_password'";
			$result = mysqli_query($conn,$query);
			if(mysqli_num_rows($result)>0)
			{
				// there is already a user with the same username
				while($row = mysqli_fetch_assoc($result)) {
					session_start();
					$_SESSION['email'] = $row["email"];
					break;
				}
				return true;
			}else{
				return false;
			}
			mysqli_close($conn);
		}
}
?>
