<?php
session_start();
$accessToken = $_SESSION['fb_access_token'];
require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$fb = new \Facebook\Facebook([
  'app_id' => '233057690927733',
  'app_secret' => 'd6b7254ca674eafb14dc83c8d2f16a80',
  'default_graph_version' => 'v3.2',
  //'default_access_token' => '{access-token}', // optional
]);
try {
  // Returns a `FacebookFacebookResponse` object
  $response = $fb->get(
    '/me',
    $accessToken
  );
} catch(FacebookExceptionsFacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(FacebookExceptionsFacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
$graphNode = $response->getGraphNode();
echo $graphNode;
?>
