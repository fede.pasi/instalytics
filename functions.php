<?php
  if (!function_exists('getIgProfile')){
    function getIgProfile($IGusername)
    {
      $get = null;
      $pos1 = 0;
      $uri = "https://www.instagram.com/";
      if($IGusername!=null && $IGusername!="")
      {
        $get = file_get_contents($uri.$IGusername);
        $find = '<script type="text/javascript">window._sharedData =';
        $pos1 = strpos($get,$find);
        $pos1 = $pos1 + strlen($find);
        //identifico la prima occorrenza di apertura del tag form
        $pos2 = strpos($get, ";", $pos1);
        //identifico la prima occorrenza di chiusura del tag form a partire da pos1
        $txt = substr($get,$pos1,$pos2-$pos1);
        $obj = json_decode($txt);
        $user = $obj->entry_data->ProfilePage[0]->graphql->user;
        // echo"<br />";
        // echo json_encode($user);
        // echo"<br />";
        return $user;
      }else{
        return false;
      }
    }
  }
  if (!function_exists('getId')){
    function getId($user){
      return $user->id;
    }
  }
  if (!function_exists('getFollower')){
    function getFollower($user){
      return $user->edge_followed_by->count;
    }
  }
  if (!function_exists('getFollowing')){
    function getFollowing($user){
      return $user->edge_follow->count;
    }
  }
  if (!function_exists('getNumMedia')){
    function getNumMedia($user){
      return $user->edge_owner_to_timeline_media->count;
    }
  }
  if (!function_exists('getUsername')){
    function getUsername($user){
      return $user->username;
    }
  }
  if (!function_exists('getFullName')){
    function getFullName($user){
      return $user->full_name;
    }
  }
  if (!function_exists('getListMedia')){
    function getListMedia($user){
      return $user->edge_owner_to_timeline_media->edges;
    }
  }
  if (!function_exists('getBiography')){
    function getBiography($user){
      return $user->biography;
    }
  }
  if (!function_exists('getBusinessCategory')){
    function getBusinessCategory($user){
      return $user->business_category_name;
    }
  }

  if (!function_exists('getProfilePic')){
    function getProfilePic($user){
      // echo("profile pic url:". $user->profile_pic_url);
      return $user->profile_pic_url;
    }
  }
  if (!function_exists('calculate_engagement_rate')){
    function calculate_engagement_rate($user){
      $c = 0;
      $follower = getFollower($user);
      $total_comment = 0;
      $total_like = 0;
      $media_comment = 0;
      $media_like = 0;
      $engagement_rate;
      $size = count($user->edge_owner_to_timeline_media->edges);
      while ($c<$size)
      {
        $comment = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_media_to_comment->count;
        $like = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_liked_by->count;
        $total_comment = $total_comment + $comment;
        $total_like = $total_like + $like;
        $c=$c+1;
      }
      $media_comment = $total_comment/$size;
      $media_like = $total_like/$size;
      $engagement_rate = (($media_comment+$media_like)/$follower)*100;
      return $engagement_rate;
    }
  }

  if (!function_exists('calculate_media_commenti')){
    function calculate_media_commenti($user){
      $c = 0;
      $follower = getFollower($user);
      $total_comment = 0;
      $media_comment = 0;
      $size = count($user->edge_owner_to_timeline_media->edges);
      while ($c<$size)
      {
        $comment = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_media_to_comment->count;
        $total_comment = $total_comment + $comment;
        $c=$c+1;
      }
      $media_comment = $total_comment/$size;
      return $media_comment;
    }
  }

  if (!function_exists('calculate_media_like')){
    function calculate_media_like($user){
      $c = 0;
      $total_like = 0;
      $media_like = 0;
      $size = count($user->edge_owner_to_timeline_media->edges);
      while ($c<$size)
      {
        $like = $user->edge_owner_to_timeline_media->edges[$c]->node->edge_liked_by->count;
        $total_like = $total_like + $like;
        $c=$c+1;
      }
      $media_like = $total_like/$size;
      return $media_like;
    }
  }

  if (!function_exists('get_first_media')){
    function get_first_media($user){
      $media = getListMedia($user);
      return $media[0]->node->display_url;
    }
  }

  if (!function_exists('calculate_vote_engagement_rate')){
    function calculate_vote_engagement_rate($follower,$engagement_rate){
      $engagement_positivo = false;
      if($follower<1000 && $engagement_rate > 8){
        $engagement_positivo = true;
      }
      if($follower>1000 && $follower<5000 && $engagement_rate > 5.7){
        $engagement_positivo = true;
      }
      if($follower>5000 && $follower<10000 && $engagement_rate > 4){
        $engagement_positivo = true;
      }
      if($follower>10000 && $follower<100000 && $engagement_rate > 2.4){
        $engagement_positivo = true;
      }
      if($follower>100000 && $engagement_rate > 1.7){
        $engagement_positivo = true;
      }
      return $engagement_positivo;
    }
  }
?>
