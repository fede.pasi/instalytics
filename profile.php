<?php
	session_start();

	if (!isset($_SESSION['loggedIn'])) {
	    header('Location: login.php');
	    exit();
	}
    require "InstagramAPI.php";

?>
<!doctype html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Instagram Log IN</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<script>
function showHint(str) {
    if (str.length == 0) {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
		var accessToken="<?php echo $_SESSION['accessToken'];?>";
        xmlhttp.open("GET", "https://api.instagram.com/v1/tags/nofilter/media/recent?access_token="+accessToken, true);
        xmlhttp.send();
    }
}
</script>
</head>
<body>
<div class="container" style="margin-top:100px;">
	<div class="row justify-content-center">
		<div class="col-md-3" align="center">
			<img src="<?php echo $_SESSION['profilePicture'] ?>">
		</div>
        <div class="col-md-9" align="center">
			ID: <?php echo $_SESSION['id']; ?><br>
			Name: <?php echo $_SESSION['fullName']; ?><br>
			Website: <?php echo $_SESSION['website']; ?><br>
			Bio: <?php echo $_SESSION['bio']; ?><br>
			ACCESS-TOKEN: <?php echo $_SESSION['accessToken']; ?><br>
			EMAIL: <?php echo $_SESSION['email']; ?><br>
		</div>
	</div>
<body>

<form>
First name: <input type="text" onkeyup="showHint(this.value)">
</form>
<p>Suggestions: <span id="txtHint"></span></p>
</div>

</body>
</html>
