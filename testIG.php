<?php
// Pass session data over. Only needed if not already passed by another script like WordPress.
session_start();
require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$fb = new \Facebook\Facebook([
  'app_id' => '233057690927733',
  'app_secret' => 'd6b7254ca674eafb14dc83c8d2f16a80',
  'default_graph_version' => 'v3.2',
  //'default_access_token' => '{access-token}', // optional
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://localhost/instalytics/fb-callback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
?>
